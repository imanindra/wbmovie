import { Map } from 'immutable';

export const FETCH_MOVIE = 'movielist/fetchMovie'
export const FETCH_MOVIE_START = 'movielist/fetchMovieStart'
export const FETCH_MOVIE_SUCCESS = 'movielist/fetchMovieSuccess'
export const FETCH_MOVIE_FILTER = 'movielist/fetchMovieFilter'
export const FETCH_MOVIE_FINISH = 'movielist/fetchMovieFinish'

export function fetchMovie() {
    return {
        type: FETCH_MOVIE,
    }
}

export function fetchMovieStart() {
    return {
        type: FETCH_MOVIE_START,
    }
}

export function fetchMovieSuccess(payload) {
    return {
        type: FETCH_MOVIE_SUCCESS,
        payload
    }
}

export function fetchMovieFilter(payload) {
    return {
        type: FETCH_MOVIE_FILTER,
        payload
    }
}

export function fetchMovieFinish() {
    return {
        type: FETCH_MOVIE_FINISH
    }
}

const initialState = Map({
    isFetching: false,
    isSuccess: false,
    data: [],
})

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_MOVIE_START:
            return state.set('isFetching', true);
        case FETCH_MOVIE_SUCCESS:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', true)
                    .set('data', action.payload)
            );
        case FETCH_MOVIE_FINISH:
            return state.withMutations(currentState =>
                currentState.set('isFetching', false).set('isSuccess', false),
            );
        default:
            return state;
    }
}

export const getIsFetching = state => state.get('isFetching');
export const getData = state => state.get('data');
export const getIsSuccess = state => state.get('isSuccess');



