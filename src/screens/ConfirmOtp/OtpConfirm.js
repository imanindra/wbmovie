import React, { Component, useState } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    View,
    Text,
    TextInput
} from 'react-native';
import Header from '../../components/HeaderMain'
import { Actions } from 'react-native-router-flux';
import HeaderText from '../../components/HeaderText';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Storage from '../../data/Storage'

class OtpConfirm extends Component {
    constructor() {
        super();
        this.state = {
            confirmResult: 555555,
            code: ''
        }
    }

    componentDidMount() {

    }

    handleVerifyCode() {
        // Request for OTP verificatioN
        const { confirmResult } = this.props
        const { code } = this.state
        if (code.length == 6) {
            confirmResult
                .confirm(code)
                .then(user => {
                    this.setState({ userId: user.uid })
                    // alert(`Verified! ${user.uid}`)
                    Storage.setToken(this.state.userId)
                    Actions.TabBar()
                })
                .catch(error => {
                    alert(error.message)
                    console.log(error)
                })
        } else {
            alert('Please enter a 6 digit OTP code.')
        }
    }

    render() {
        return (
            <SafeAreaView style={[styles.container, { backgroundColor: 'white' }]}>
                <HeaderText
                    Title={'Verify your phone number'}
                />
                <Text>Enter the 6-digit code we sent to</Text>
                <OTPInputView
                    style={{ width: wp(60), height: hp(20), alignSelf: 'center' }}
                    pinCount={6}
                    // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                    // onCodeChanged = {code => { this.setState({code})}}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled={(code => {
                        this.setState({
                            code
                        }, () => this.handleVerifyCode())

                    })}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity>
                        <Text style={{textDecorationLine : 'underline'}}>Persyaratan Layanan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{textDecorationLine : 'underline'}}>Kebijakan Privasi</Text>
                    </TouchableOpacity>

                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "black",
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: 'black'
    },

    underlineStyleHighLighted: {
        borderColor: "black",
    },
});


export default OtpConfirm 
