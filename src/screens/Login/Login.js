import React, { Component, useState } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    View,
    Text,
    TextInput
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import firebase from '@react-native-firebase/app';
import {auth} from '@react-native-firebase/auth';
import HeaderText from '../../components/HeaderText';
import PhoneInput from 'react-native-phone-input'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Constant from '../../constant/constant'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            phone: '',
            confirmResult: null,
            verificationCode: '',
            userId: '',
            valid: "",
            type: "",
            value: "",
            tess: '+62'
        }
        this.updateInfo = this.updateInfo.bind(this);
    }

    componentDidMount() {
        firebase.initializeApp()
    }

    updateInfo() {
        this.setState({
            valid: this.phone.isValidNumber(),
            type: this.phone.getNumberType(),
            value: this.phone.getValue()
        });
    }

    validatePhoneNumber = () => {
        var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/
        return regexp.test(this.state.tess)
    }

    handleSendCode() {
        if (this.validatePhoneNumber()) {
            firebase
                .auth()
                .signInWithPhoneNumber(this.state.tess)
                .then(confirmResult => {
                    this.setState({ confirmResult },
                        () => Actions.Otp({ confirmResult: this.state.confirmResult }))
                })
                .catch(error => {
                    alert(error.message)
                    console.log(error)
                })
        } else {
            alert('Invalid Phone Number')
        }
    }

    changePhoneNumber = () => {
        this.setState({ confirmResult: null, verificationCode: '' })
    }

    handleVerifyCode = () => {
        // Request for OTP verification
        const { confirmResult, verificationCode } = this.state
        if (verificationCode.length == 6) {
            confirmResult
                .confirm(verificationCode)
                .then(user => {
                    this.setState({ userId: user.uid })
                    alert(`Verified! ${user.uid}`)
                    Actions.TabBar()
                })
                .catch(error => {
                    alert(error.message)
                    console.log(error)
                })
        } else {
            alert('Please enter a 6 digit OTP code.')
        }
    }

    renderConfirmationCodeView = () => {
        return (
            <View style={styles.verificationView}>
                <TextInput
                    style={styles.textInput}
                    placeholder='Verification code'
                    placeholderTextColor='#eee'
                    value={this.state.verificationCode}
                    keyboardType='numeric'
                    onChangeText={verificationCode => {
                        this.setState({ verificationCode })
                    }}
                    maxLength={6}
                />
                <TouchableOpacity
                    style={[styles.themeButton, { marginTop: 20 }]}
                    onPress={this.handleVerifyCode}>
                    <Text style={styles.themeButtonTitle}>Verify Code</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={[styles.container, { backgroundColor: 'white' }]}>
                <HeaderText
                    Title={'Enter your phone number'}
                />
                <View style={styles.page}>

                    <View style={{ borderWidth: 1, height: hp(5), width: wp(60), justifyContent: 'center' }}>
                        <PhoneInput
                            style={{ marginHorizontal: wp(2) }}
                            ref={(ref) => { this.phone = ref; }}
                            initialCountry='id'
                            onSelectCountry={() => this.setState({ tess: this.phone.getValue() })}
                            value={this.state.tess}
                            onChangePhoneNumber={tess => {
                                this.setState({
                                    tess
                                })
                            }}
                        />
                    </View>
                    <View style={{ width: '80%', marginTop: hp(5) }}>
                        <Text>
                            By tapping "Verify Phone Number", you are indicating that
                            you accept our
                            <Text
                                style={{ color: 'green', textDecorationLine: 'underline' }}
                            > Terms of Service </Text>
                            and
                            <Text
                                style={{ color: 'green', textDecorationLine: 'underline' }}
                            > Privacy Policy</Text>. An SMS may be sent.
                            Message & data rates nat apply.
                        </Text>
                    </View>

                    <TouchableOpacity
                        style={[styles.themeButton, { marginTop: 20 }]}
                        onPress={() => this.handleSendCode()}>
                        <Text style={styles.themeButtonTitle}>
                            {'Verification Phone Number'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#aaa'
    },
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        marginTop: 20,
        width: '90%',
        height: 40,
        borderColor: '#555',
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 10,
        color: '#fff',
        fontSize: 16
    },
    themeButton: {
        width: wp(80),
        height: hp(6),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Constant.COLOR.SECONDARY,
        borderRadius: 5
    },
    themeButtonTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#fff'
    },
    verificationView: {
        width: '100%',
        alignItems: 'center',
        marginTop: 50
    }
})


export default Login 
