import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import Home from '../Home/Home'
import Series from '../Series/Series'
import Anime from '../Anime/Anime'
import Movies from '../Movies/Movies'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../../components/HeaderMain'
import remoteConfig from '@react-native-firebase/remote-config';

const FirstRoute = () => (
    <Home />
);
const SecondRoute = () => (
    <Movies />
);
const ThirdRoute = () => (
    <Series />
);
const FourRoute = () => (
    <Anime />
);


export default class TabViewExample extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'HOME' },
            { key: 'second', title: 'MOVIES' },
            { key: 'third', title: 'SERIES' },
            { key: 'four', title: 'ANIME' },
        ],
    };

    async componentDidMount() {
        //    firebase.initializeApp()
        remoteConfig()
            .setDefaults({
                series: 'disabled',
            })
            .then(() => remoteConfig().fetchAndActivate())
            .then(activated => {
                if (activated) {
                    console.log('Defaults set, fetched & activated!');
                } else {
                    console.log('Defaults set, however activation failed.');
                }
            });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header/>
                <TabView
                    swipeEnabled = {false}
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: FirstRoute,
                        second: SecondRoute,
                        third: ThirdRoute,
                        four: FourRoute
                    })}

                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    renderTabBar={props =>
                        <TabBar
                            {...props}
                            style={{ width: wp(100), alignSelf: 'center', backgroundColor: 'white' }}
                            labelStyle={{color : 'black'}}
                            // indicatorStyle={{ backgroundColor: constant.NEWCOLOR.BLUESECONDARY }}
                        />

                    }
                    style={styles.container}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // marginTop: StatusBar.currentHeight,
    },
    scene: {
        flex: 1,
    },
});
