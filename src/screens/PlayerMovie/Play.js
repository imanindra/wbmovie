import React, { Component } from 'react'
import { View, StyleSheet, BackHandler, ActivityIndicator, StatusBar, TouchableOpacity, Image, Text } from 'react-native'
import { WebView } from 'react-native-webview';
import { Actions } from 'react-native-router-flux';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class WebViewGame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false
        }
    }

    componentDidMount() {
        const { Play } = this.props
        console.log('play',Play)
    }

    render() {
        const { Play } = this.props
        return (
            <View style={{ flex: 1 }}>
                <WebView
                    source={{ uri: `${Play}` }}
                    startInLoadingState={true}
                />
            </View>
        )
    }
}

export default WebViewGame