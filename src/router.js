import React, { Component } from 'react';
import { View, BackHandler, Image, AppRegistry } from 'react-native';
import { Scene, Stack, Router, Tabs, Actions } from 'react-native-router-flux';
import Storage from './data/Storage';
import Splash from './screens/SplashScreen/SplashScreen'
import Login from './screens/Login/Login';
import Home from './screens/Home/Home';
import Detail from './screens/Detail/Detail';
import OTP from './screens/ConfirmOtp/OtpConfirm';
import Play from './screens/PlayerMovie/Play';
import TabBar from './screens/Home/TabBar';
import DetailSeries from './screens/Detail/DetailSeries';
import DetailAnime from './screens/Detail/DetailAnime';
import Genre from './screens/Genre/Genre';
import Search from './screens/Search/Search';

class router extends React.Component {
    state = {
        isInitiated: false,
        token: null,
    }

    async UNSAFE_componentWillMount() {
        await Storage.getToken().then(token => {
            if (token) {
                this.setState({
                    token: token.accessToken,
                    isInitiated: true,
                });
                // console.log('NULL', token)
            } else {
                this.setState({ isInitiated: true });
            }
        });
    }

    render() {
        const { token, isInitiated } = this.state;
        if (!isInitiated) {
            return null;
          }
        return (
            <View style={{ flex: 1 }}>
                <Router>
                    <Scene key="root">
                        <Scene key="Splash" component={Splash} hideNavBar />
                        <Scene key="Login"  initial={token} component={Login} hideNavBar />
                        <Scene key="TabBar" component={TabBar} hideNavBar />
                        <Scene key="Home" component={Home} hideNavBar />
                        <Scene key="Detail" component={Detail} hideNavBar />
                        <Scene key="DetailSeries" component={DetailSeries} hideNavBar />
                        <Scene key="DetailAnime" component={DetailAnime} hideNavBar />
                        <Scene key="Play" component={Play} hideNavBar />
                        <Scene key="Otp" component={OTP} hideNavBar />
                        <Scene key="Genre" component={Genre} hideNavBar />
                        <Scene key="Search" component={Search} hideNavBar />
                    </Scene>
                </Router>
            </View>

        )
    }
}

export default router;