import React from 'react';
import { SafeAreaView, Alert, BackHandler, View, Text, StyleSheet, TouchableOpacity, Image, AppRegistry, Linking } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import AppRouter from './router';
import { store, persistor } from './configureStore';
// import firebase from '@react-native-firebase/app';
import remoteConfig from '@react-native-firebase/remote-config';

console.disableYellowBox = true;

class root extends React.Component {
    state = {
        isModalVisible: false,
        active: true
    };

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    async componentDidMount() {
        //    firebase.initializeApp()
        remoteConfig()
            .setDefaults({
                series: 'disabled',
            })
            .then(() => remoteConfig().fetchAndActivate())
            .then(activated => {
                if (activated) {
                    console.log('Defaults set, fetched & activated!');
                } else {
                    console.log('Defaults set, however activation failed.');
                }
            });
    }

    async UNSAFE_componentWillMount() {
        this.setState({
            isModalVisible: false
        })
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible, isLoading: false });
    };

    render() {
        const { isModalVisible } = this.state
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
                        <AppRouter />
                    </SafeAreaView>
                </PersistGate>
            </Provider>
        );
    }
}

export default root;
AppRegistry.registerComponent('root', () => root);

