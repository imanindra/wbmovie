import { store } from '../configureStore';
import {
    defaultHeaders,
    buildResponse,
    buildResponseNew,
    buildHeaders,
    buildParam,
    BASE_URL,
    buildResponseLogin,
    buildHeadersImage,
} from './apiHelper';

import { ERROR_TAG } from '../redux/error';

// getMovie
export function getMovie() {
    return fetch(`${BASE_URL}movie/newest?page=`)
        .then(response => buildResponse(response, "ERROR MOVIE INFO"))
        .then(data => data)
}

// getSeries
export function getSeries() {
    return fetch(`${BASE_URL}movie/drama?page=`)
        .then(response => buildResponse(response, "ERROR SERIE INFO"))
        .then(data => data)
}


// getMovie
export function getAnime() {
    return fetch(`${BASE_URL}animes/newest?page=`)
        .then(response => buildResponse(response, "ERROR ANIME INFO"))
        .then(data => data)
}



