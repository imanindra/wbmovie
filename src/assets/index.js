export default {
    //Logo App
    icon_splash : require('./ic_wbmovielogo.png'),
    icon_logo : require('./ic_wbmovie.png'),
    icon_play : require('./ic_play_button.png'),
    icon_share : require('./ic_share.png'),
    icon_arrow : require('./ic_arrow.png'),
    icon_playlist : require('./ic_playlist.png'),
    icon_menu : require('./ic_menu.png'),
    icon_search : require ( './ic_search.png')
}
