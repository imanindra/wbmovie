import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, Alert } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        height: height * 0.065,
        backgroundColor: Constant.COLOR.PRIMARY,
        alignItems: 'center',
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
})

class HeaderGenre extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container} elevation={1}>
                <View style={[styles.titleContainer, { alignContent: 'center' }]}>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()}
                            style={{
                                width: wp(7),
                                height: wp(5),
                                resizeMode: 'contain',
                                left: wp(-24),
                                alignSelf: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                style={{ width: wp(7), height: wp(5), resizeMode: 'contain' }}
                                source={assets.icon_menu}
                            />
                        </TouchableOpacity>
                        <Image
                            style={{ width: wp(30), height: wp(12), resizeMode: 'contain' }}
                            source={assets.icon_logo}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

export default HeaderGenre
