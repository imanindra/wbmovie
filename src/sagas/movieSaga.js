import { call, put, fork, take } from 'redux-saga/effects';
import {
    FETCH_MOVIE,
    fetchMovieStart,
    fetchMovieFinish,
    fetchMovieSuccess,
} from '../redux/movielist';
import { setErrorMessage } from '../redux/error';
import { getMovie } from '../services/api';

function* movieSaga() {
    while (true) {
        const action = yield take(FETCH_MOVIE);
        try {
            yield put(fetchMovieStart());
            const movieData = yield call(
                getMovie
            );

            if (__DEV__) console.log("Data MOVIE is ", movieData);

            yield put(fetchMovieSuccess(movieData))
        } catch (err) {
            console.log('ERROR GET MOVIE DATA ', err);
            fetchMovieFinish(err)
        }
    }
}


export default function* wacher() {
    yield fork(movieSaga);
}

