import {call, put, fork, take} from 'redux-saga/effects';
import {LOGIN, loginFinish, loginStart, loginSuccess} from '../redux/auth';
// import {fetchProfileSuccess} from '../redux/profile'
import {setErrorMessage} from '../redux/error';
import Storage from '../data/Storage';
import { Actions } from 'react-native-router-flux';

function* loginSaga() {
  while (true) {
    const action = yield take(LOGIN);
    try {
      const {username, password} = action.payload;
      yield put(loginStart());
      const loginData = yield call(login, username, password);

      if (__DEV__) {
        console.log('Login data is ', loginData);
      }

      if (loginData.message) {
        yield put(loginFinish(loginData.message));
        yield put(setErrorMessage(loginData.message == 'Too many failed login attempt. Please verify your phone number to unlock this account.' ? Actions.UnlockPhone() : loginData.message));
      } else {
        Storage.setToken(loginData.api_token);
        Storage.setUser(loginData);

        // yield put(fetchProfileSuccess(loginData))
        yield put(loginSuccess(loginData));

        
      }
    } catch (err) {
      console.log('ERROR LOGIN ', err);
    }
  }
}

export default function* wacher() {
  yield fork(loginSaga);
}
