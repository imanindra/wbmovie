/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import root from './src/root';

AppRegistry.registerComponent(appName, () => root);
